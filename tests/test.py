import filecmp
import subprocess
import shlex
import os
from inspect import getsourcefile


total_tests = 6

# script_dir is the directory crossref_bibtex.py is located in
script_dir = os.path.dirname(getsourcefile(lambda:0))
# install_directory is the directory the holds py/crossref_bibtex.py
install_directory = os.path.dirname(script_dir)

# The path to a test config file relative to the scraper's installed directory
config_path = "tests/test_config.txt"
# The path of crossref_bibtex.py relative to the scraper's installed directory
scraper_path = os.path.join(install_directory, "py/crossref_bibtex.py")
citation_sort_path = os.path.join(install_directory, "tests/sort_citations.py")

# Paths to two temporary files used by the scraper
temp_file_path = os.path.join(install_directory, "test.bib")
correct_temp_path = os.path.join(install_directory, "correct.bib")


# Prints which test is running
def print_test_running(test_num, total_tests):
    print ("Running test " + str(test_num) + "/" + str(total_tests))


# Prints the result of a test
def print_test_outcome(correct_citation, test_num, total_tests):
    if not correct_citation:
        print ("Wrong citation generated for test " + str(test_num) + "/" + str(total_tests))
    else:
        print ("Test " + str(test_num) + "/" + str(total_tests) + " passed\n")


# Adds quotation marks around a string
def quote(string):
    return "\"" + string + "\""


def run_test(install_directory, arg, correct_file, config_path, test_num, total_tests):
    print_test_running(test_num, total_tests)
    correct_file = os.path.join(install_directory, correct_file)

    # The command to run for this test
    command = "python " + quote(scraper_path) + " " + arg + " -f " +  quote(temp_file_path) + " -c " + quote(config_path)

    args = shlex.split(command)
    subprocess.call(args)

    # Sort the test and correct citations so they can be compared
    subprocess.call(["python", citation_sort_path, temp_file_path])
    subprocess.call(["python", citation_sort_path, correct_file, correct_temp_path])

    is_correct_citation = filecmp.cmp(temp_file_path, correct_temp_path)
    print_test_outcome(is_correct_citation, test_num, total_tests)

    if is_correct_citation:
        return 1
    else:
        # We quit whenever a test fails so that the user can compare the generated citation to the correct citation
        print ("Quitting on failed test")
        print ("Incorrect citation is at: " + temp_file_path + " Correct citation is at: " + correct_temp_path)
        quit()


# Test case 1
test_num = 1
correct_file = "tests/IEEETSP-y2016-i1.bib"
arg = "IEEE\ TSP -y 2016 -m 1 -i 1"
run_test(install_directory, arg, correct_file, config_path, test_num, total_tests)

# Test case 2
test_num = 2
correct_file = "tests/IEEETSP-y2014-m4.bib"
arg = "IEEE\ TSP -y 2014 -m 4"
run_test(install_directory, arg, correct_file, config_path, test_num, total_tests)

# Test case 3
test_num = 3
correct_file = "tests/IEEETSP-d01_2011:06_2012.bib"
arg = "IEEE\ TSP -d 01/2011 06/2012"
run_test(install_directory, arg, correct_file, config_path, test_num, total_tests)

# Test case 4
test_num = 4
correct_file = "tests/SIDMA-y2011-m1-i1,2.bib"
arg = "SIDMA -y 2011 -m 1 -i 1 2"
run_test(install_directory, arg, correct_file, config_path, test_num, total_tests)

# Test case 5
# Tests to see if the scraper works on journals with articles
# listed under only one of the journals' issns.
test_num = 5
correct_file = "tests/wileyMRM-y2016-i5.bib"
arg = "wiley-mrm -y 2016 -i 5"
run_test(install_directory, arg, correct_file, config_path, test_num, total_tests)

# Test case 6
# Tests to see if articles can be successfully downloaded
# from the journal Physics in Medicine and Biology
test_num = 6
correct_file = "tests/PMB-y2016-i23.bib"
arg = "0031-9155 -y 2016 -i 23"
run_test(install_directory, arg, correct_file, config_path, test_num, total_tests)

os.remove(temp_file_path)
os.remove(correct_temp_path)
