import os, sys, re

doi_regex = re.compile("@article{(.*?),")


# If the file to sort doesn't exist quit
if not os.path.exists(sys.argv[1]):
    print ("Can't find file " + sys.argv[1] + " to sort its citations")
    quit()


def sort_citations(read_file, write_file):
    citations = read_file.read()
    citations = citations.split('\n\n')
    citations.pop()
    citations = sorted(citations, key = lambda citation: doi_regex.search(citation).group(1))
    out_str = ""
    for citation in citations:
        out_str += citation + "\n\n"

    write_file.seek(0) # Takes care of the scenario where read_file is the same as write_file
    write_file.write(out_str)
    write_file.truncate()


if len(sys.argv) == 2:
    file_path = sys.argv[1]
    with open(file_path, 'r+') as f:
        sort_citations(f, f)

elif len(sys.argv) == 3:
    read_path = sys.argv[1]
    write_path = sys.argv[2]
    with open(read_path, 'r') as r:
        with open(write_path, 'w') as w:
            sort_citations(r, w)

else:
    print ("Usage: python sort_citations.py FILE_TO_SORT [WRITE_PATH]")
