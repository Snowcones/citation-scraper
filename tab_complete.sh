_scraper()
{
    title_script="py/get_journal_titles.py"

    local iter cur
    is_quoted=0
    COMPREPLY=()

    if [[ $COMP_CWORD -eq 1 ]]; then                # If the first arg is being typed, run journal autocomplete
        cur="${COMP_WORDS[COMP_CWORD]}"             # Gets the current arg being typed

        # Here we handle a quoted journal title by converting it to an unquoted title
        # then we can handle the quoted and unquoted journal titles the same
        if [[ $cur =~ ^\" || $cur =~ ^\' ]]; then
            cur="${cur/\"/}"    # Strips leading double quote
            cur="${cur/\'/}"    # Strips leading single quote
            cur="${cur/%\"/}"   # Strips trailing double quote
            cur="${cur/%\'/}"   # Strips trailing single quote
            cur="${cur// /\\ }" # Escapes the spaces in the string
            is_quoted=1
        fi

        # If the cursor is past the end of a quoted argument, tab-complete returns an unquoted suggestion
        # i.e. "IEEE TS" with the cursor past the second quote, tab completed changes it to IEEE\ TSP
        if [[ ${COMP_LINE:$COMP_POINT:1} = "" && ${COMP_LINE:$[ $COMP_POINT - 1 ]:1} = "\"" && is_quoted -eq 1 ]]; then
            is_quoted=0
        fi

        cur="${cur//\\ /\___}"
        opts="$(python $title_script)"              # Get the available journal titles from scraper_config.txt
        opts="${opts//\\ /\___}"                    # Substitute "\ " in journal titles with "\___" to bypass odd treatment of spaces by the complete command
        for iter in $opts; do                       # If the arg being typed matches a journal title add the journal title to the list of tab-complete suggestions
            if [[ "$iter" =~ ^"$cur" ]]; then
                if [[ $is_quoted -eq 0 ]]; then
                    COMPREPLY+=( "${iter//\___/ }" )
                else
                    unescaped=( "${iter//\___/ }" )
                    COMPREPLY+=( "${unescaped//\\ / }" ) # This substitution gives us a tab-complete suggestion in the quoted format
                fi
            fi
        done
    fi
} &&
complete -F _scraper scraper
