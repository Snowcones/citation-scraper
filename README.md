# CrossRef Citation Scraper

This scraper uses the CrossRef API to generate bibtex citations from any journal that is registered with CrossRef registration agency.

# Synopsis

To run this scraper, cd into the "Crossref Scraper" folder and run:
```
$ ./scraper journal_identifier [-h] (-y year | -d date(s)) [-m month] [-i issue(s)] [-f file] [-c config_path]
```

The program requires the following argument:  

* **journal_identifier**  
Either the ISSN of a journal or a journal title listed in config.txt  

The program takes the following optional arguments:

* **-h**  
Shows the scaper's help message.

* **-y** year  
The year to get citations from in the form **YYYY**.  

* **-d** date(s)
Either a single date of the form **MM/YYYY** specifying a month to get citations from, or a start and end date of the form **MM/YYYY MM/YYYY** specifying a range of months to get citations from.  

* **-i** issue1 issue2 ...  
The issue number or issue numbers to download citations for.  

* **-m** month  
The month to get citations from in the form **MM**. This argument can only be used with the **-y** flag.  

* **-f** file  
The path to write the citations to, relative to your current working directory. This defaults to "file.bib".  

* **-c** config_path  
The path to the config file to use for the scraper, relative to the scraper's install directory. This defaults to "config.txt".  

# Config File:

The scaper's config file allows the scraper to lookup a journal's ISSN given an identifying string for the journal. By default the scraper uses "config.txt" as the config file. "config.txt" initially has two entries as examples. The config file if edited should have the format:
```
JOURNAL_IDENTIFIER: JOURNAL_ISSN  
JOURNAL_IDENTIFIER: JOURNAL_ISSN  
⋮
```
# Example Usage:
```
$ ./scraper "IEEE TSP" -d 1/2013 12/2013
```
Will write the citation of every article published in IEEE Transactions on Signal Processing during 2013 to the path file.bib.
```
$ ./scraper SIDMA -y 2015 -i 1 2 3 -f siam_2015.bib
```
Will write the citation of every article published in issues 1, 2, and 3 of the SIAM Journal of Discrete Mathematics 2015, during 2015, to the path siam_2015.bib.

# Tab-Complete:

The file tab_complete.sh provides tab-complete for journal titles in bash. To load this functionality for the current session, run:
```
$ source tab_complete.sh
```
This script was written to run with bash and likely won't work for with other shells. To automatically load this functionality, add the line:
```
$ source PATH_TO_TAB_COMPLETE.SH
```
to your ~/.bashrc file

# Shells Other Than Bash

The executable "scraper" is a bash script that runs a python script. If "scraper" fails to run in a shell that isn't bash, the python scraper file can be run directly. To do so cd into the "Crossref Scraper" folder and run:
```
$ python py/crossref_bibtex.py journal_identifier [-ym|-d][-fi]
```


This work was supported in part by a grant  
from the W M Keck Foundation  
to EECS Professors J. Fessler, T. Norris, and Z. Zhong  
at the University of Michigan.
