from formatting_helpers import *

# This file contains helper functions that take a dictionary representing
# a CrossRef article and return specific information about that article
# if the dictionary contains that info

def article_authors(article):
    try:
        authors = article["author"]
        formatted_authors = format_authors(authors)
    except:
        formatted_authors = None
    return formatted_authors

def article_journal(article):
    try:
        journal = article["container-title"][0]
    except:
        print ("article has no journal")
        journal = None
    return journal

def article_title(article):
    try:
        title = article["title"][0]
    except:
        title = None
    return title

def article_year(article):
    try:
        year = article["published-print"]["date-parts"][0][0]
    except:
        year = None
    return year

def article_volume(article):
    try:
        volume = article["volume"]
    except:
        volume = None
    return volume

def article_issue(article):
    try:
        issue = article["issue"]
    except:
        issue = None
    return issue

def article_pages(article):
    try:
        pages = article["page"]
        page_range = pages.split("-")
    except:
        page_range = None

    try:
        page_start = page_range[0]
    except:
        page_start = None

    try:
        page_end = page_range[1]
    except:
        page_end = None
    return page_start, page_end

def article_doi(article):
    try:
        doi = article["DOI"]
    except:
        print ("No DOI found")
        quit()
        doi = None
    return doi

def article_issn(article):
    try:
        issn = article["ISSN"][0]
    except:
        issn = None
    return issn

def article_month(article):
    try:
        month_num = article["published-print"]["date-parts"][0][1]
        month = month_string(month_num)
    except:
        month = None
    return month
