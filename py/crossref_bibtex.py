import re, json, os, sys, socket, argparse, traceback
version = sys.version_info.major
if version == 2:
    import urllib2
elif version == 3:
    import urllib.request

from inspect import getsourcefile
from query_helpers import *
from formatting_helpers import *

MAX_ITEMS_PER_PAGE = 1000  # The max number of results the Crossref API will return in a single query


# This function checks to see whether the user has internet connection
def have_connection():
    try:
        # Try to connect to Google's DNS servers
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("8.8.8.8", 53))
        return True
    except socket.error:
        # If the user was unable to reach Google's DNS servers
        # chances are they have no internet
        return False


def url_open_string(url):
    if verbose:
        print("Querying: " + url)
    if version == 2:
        return urllib2.urlopen(url, timeout=30).read()
    elif version == 3:
        request = urllib.request.urlopen(url, timeout=30)
        return request.read().decode(request.headers.get_content_charset())


# Prints an error if more than two dates were passed to the scraper
def too_many_dates(dates):
    print ("Passed too many dates to scraper: " + str(dates))
    print ("Call scraper with -h to get usage info")
    quit()


# Gets the search dates for the query given the various possible command line arguments
def handle_dates(dates, year, month):
    if dates is not None:
        if len(dates) > 2:
            too_many_dates(dates)
        return parse_dates(dates)
    else:
        return parse_year(year, month)


# Prints an error if a date is incorrectly formatted
def unsupported_date(date):
    print ("Unsupported date format: " + date)
    print ("Call scraper with -h to get usage info")
    quit()


# Takes one or two dates of the form "MM/YYYY" and returns a start date and end
# date in the form ['M', 'Y']
def parse_dates(dates):
    return_dates = []
    if len(dates) == 1:
        date_parts = dates[0].split("/")
        return_dates.append(date_parts)
        return_dates.append(date_parts)
    elif len(dates) == 2:
        for date in dates:
            date_parts = date.split("/")
            return_dates.append(date_parts)
    else:
        unsupported_date(date)
    return return_dates[0], return_dates[1]


# Takes a year and optionally a month and returns a start and end date of the
# form ['M, 'Y']
def parse_year(year, month):
    if month is not None:
        return [month, year], [month, year]
    else:
        return ["1", year], ["12", year]


# Returns the issn of a journal given its identifier (either its issn or an
# entry in config.txt)
def get_issn(journal_identifier, config_path):
    is_hyphenated_issn = len(journal_identifier) == 9 and re.match(r"\d{4}-\d{3}[\dxX]", journal_identifier)
    is_unhyphenated_issn = len(journal_identifier) == 8 and re.match(r"\d{7}[\dxX]", journal_identifier)
    if is_hyphenated_issn or is_unhyphenated_issn:
        return  journal_identifier
    else:
        return lookup_issn_in_config(journal_identifier, config_path)


# Gets the issn for a journal title if that journal is in config.txt
# otherwise returns None
def lookup_issn_in_config(journal_title, config_path):
    # These regexes search the config file for the given journal and return its ISSN
    unhyphenated_regex = journal_title + r"\s*:\s*(\d{7}[\dxX])"
    hyphenated_regex = journal_title + r"\s*:\s*(\d{4}-\d{3}[\dxX])"

    with open(config_path, "r") as journal_config:
        known_journals = journal_config.read()
        if re.search(unhyphenated_regex, known_journals, re.IGNORECASE) is not None:
            return re.search(unhyphenated_regex, known_journals, re.IGNORECASE).group(1)
        elif re.search(hyphenated_regex, known_journals, re.IGNORECASE) is not None:
            return re.search(hyphenated_regex, known_journals, re.IGNORECASE).group(1)
        else:
            return None


# This function takes an issn, an amount of results per page, a start index, a start date, and an end date,
# and returns a URL corresponding to that query to the CrossRef API
def generate_request_url(issn, results_per_page, start_index, start_date, end_date):
    start_month = int(start_date[0])
    start_year = int(start_date[1])
    end_month = int(end_date[0])
    end_year = int(end_date[1])

    request_url = "http://api.crossref.org/journals/{" + issn + "}/works?rows=" + str(results_per_page) + "&offset=" + str(start_index) + "&filter="

    # Verifies that the start month given is valid and puts it in the request url
    if start_month < 1 or start_month > 12:
        print ("Bad start month argument")
        quit()
    else:
        request_url += "from-print-pub-date:" + str(start_year).zfill(4) + "-" + str(start_month).zfill(2)

    if end_month < 1 or end_month > 12:
        print ("Bad end month argument")
        quit()
    else:
        request_url += ",until-print-pub-date:" + str(end_year).zfill(4) + "-" + str(end_month).zfill(2)

    return request_url


# Gets the total number of articles in a json object representing a CrossRef query
def get_num_articles(json_page):
    if json_page["status"] != "ok":
        return None
    message = json_page["message"]
    total_results = int(message["total-results"])
    return total_results


# Gets the name of a journal, according to the CrossRef database, from a json object
# representing a CrossRef query
def get_journal_title(json_page):
    if json_page["status"] != "ok":
        return None
    message = json_page["message"]
    try:
        articles = message["items"]
        return articles[0]["container-title"][0]
    except Exception:
        return None


# Returns a list of all of a journal's ISSNs and returns the title of the
# journal given a single one of the journal's ISSNs
def get_journal_info(issn):
    query_url = "http://api.crossref.org/journals/{" + str(issn) + "}"
    try:
        page_text = url_open_string(query_url)
        json_page = json.loads(page_text)
        if json_page["status"] != "ok":
            return None
        message = json_page["message"]
        issns = message["ISSN"]
        title = message["title"]
        return issns, title
    except urllib2.HTTPError as err:
        print ("Can't find a journal with ISSN: " + str(issn) + " on Crossref.org")
        quit()
    except Exception as e:
        if have_connection():
            print (traceback.format_exc())
        else:
            print ("Unable to reach the internet, check your connection")
        quit()


# Returns the json object returned from a request to request_url
def load_json_page(request_url):
    page_text = url_open_string(request_url)
    json_page = json.loads(page_text)
    return json_page


# Prints info about what the scraper will download
def print_query_info(issues, journal_title, start_date, end_date, write_path):
    print ("Downloading citiations from journal: " + journal_title)
    if issues is not None:
        print ("in issue(s): " + format_list(issues))
    print ("starting from: " + format_date(start_date))
    print ("and ending at: " + format_date(end_date))
    print ("and saving them to: " + write_path)


# Returns a JSON page holding the results of a query to the crossref api
def make_query(issn, results_per_page, start_index, start_date, end_date):
    query_url = generate_request_url(issn, results_per_page, start_index, start_date, end_date)
    page = load_json_page(query_url)
    return page


# Returns the json object for a Crossref search in addition to the number of articles in that search
# and the title of the journal you're searching
def make_initial_query(issn, items_per_page, start_date, end_date):
    page = make_query(issn, items_per_page, 0, start_date, end_date)
    return page, get_num_articles(page)


# Takes an articles's CrossRef listing and converts it into a string containing a bibtex citation
# this function will intentionally crash if the article is missing a DOI
def format_article(article):
    try:
        item_type = article["type"]
        if item_type == "journal-issue":
            return "" # Journal issues don't have meaningful info
        elif item_type != "journal-article": # TODO add support for more citation types
            print ("Not generating citation for unknown item of type: " + item_type)
            return ""
    except Exception:
        item_type = None

    formatted_authors = article_authors(article)
    journal = article_journal(article)
    title = article_title(article)
    year = article_year(article)
    volume = article_volume(article)
    issue = article_issue(article)
    page_start, page_end = article_pages(article)
    doi = article_doi(article)
    issn = article_issn(article)
    month = article_month(article)

    citation = "@article{doi:" + str(doi) + ",\n"
    if formatted_authors is not None:
        citation += "author={" + formatted_authors + "},\n"
    if journal is not None:
        citation += "journal={" + journal + "},\n"
    if title is not None:
        citation += "title={" + title + "},\n"
    if year is not None:
        citation += "year={" + str(year) + "},\n"
    if volume is not None:
        citation += "volume={" + str(volume) + "},\n"
    if issue is not None:
        citation += "number={" + str(issue) + "},\n"
    if page_start is not None and page_end is not None:
        citation += "pages={" + str(page_start) + "-" + str(page_end) + "},\n"
    elif page_start is not None:
        citation += "pages={" + str(page_start) + "-},\n"
    if doi is not None:
        citation += "doi={" + str(doi) + "},\n"
    if issn is not None:
        citation += "ISSN={" + str(issn) + "},\n"
    if month is not None:
        citation += "month={" + month + "},\n"
    citation = citation.rstrip()
    citation += "}\n\n"
    return citation


# Writes the citations generated from a CrossRef query to a file
# json_page is the json object returned from a CrossRef query
# write_file is the path to write the generated citations to
# issues is an option array of issue numbers to filter by
def write_citations_for_page(json_page, downloaded_citations, write_file, issues):
    message = json_page["message"]
    articles = message["items"]
    for a in articles:
        try:
            # Checks if the articles matches our requested issue(s_
            if issues is None or a["issue"] in issues:
                # Checks if we haven't seen this article already
                if a["DOI"] not in downloaded_citations:
                    citation = format_article(a)
                    if citation != "":
                        if version == 2:
                            write_file.write(citation.encode("utf-8"))
                        elif version == 3:
                            write_file.write(citation)
                        downloaded_citations.add(a["DOI"])
        except Exception:
            pass


# Downloads all of the citations in a list of ISSNs to a file
def write_citations_for_issns(journal_issns, issues, journal_title, items_per_page, start_date, end_date, write_path):
    # First we print (info about what we're going to download)
    print_query_info(issues, journal_title, start_date, end_date, write_path)

    # We use this set to make sure we only download one citation for each aritcle we find
    # when searching through each of the journal's ISSNs
    downloaded_citations = set()

    # Here we iterate through each of the journal's ISSNs and download all the citations we find
    with open(write_path, 'w') as write_file:
        for issn in journal_issns:
            page, total_items = make_initial_query(issn, items_per_page, start_date, end_date)
            write_citations_for_page(page, downloaded_citations, write_file, issues)

            start_item = 0
            while start_item < total_items:
                start_item += items_per_page
                page = make_query(issn, items_per_page, start_item, start_date, end_date)
                write_citations_for_page(page, downloaded_citations, write_file, issues)

    print_download_summary(downloaded_citations, write_path)


# Prints a summary of the number of citations we downloaded
def print_download_summary(downloaded_citations, write_path):
    print ("Finished downloading: " + str(len(downloaded_citations)) + " citations to the file: " + write_path)


# Returns an argparse parser for this script's command line interface
def get_parser():
    parser = argparse.ArgumentParser(prog="scraper")
    parser.add_argument("Journal_Identifier",
                        help="The journal identifier")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-y", metavar="year",
                       help="The year in the form YYYY to get citations for")
    group.add_argument("-d", nargs="*", metavar="date",
                       help="Either a single date of the form MM/YYYY or a start and end"
                       " date of the form MM/YYYY MM/YYYY specifying what date(s) to get"
                       " citations from.")
    parser.add_argument("-m", metavar="month",
                        help="The month in the form MM to get citations for")
    parser.add_argument("-i", metavar="issue", nargs="*",
                        help="The issue(s) to get citations for")
    parser.add_argument("-f", metavar="file", default="file.bib",
                        help="The file to write citations to, relative to your current"
                        " working directory")
    parser.add_argument("-c", metavar="config_path", default="config.txt",
                        help="The path to the scraper's config file relative to the"
                        " scraper's install directory")
    parser.add_argument("-v", dest="verbose_mode", action='store_true',
                        help="Makes the scraper print all of the urls it queries")
    return parser


def main(args):
    global verbose
    parser = get_parser()
    args = parser.parse_args(args)

    start_date, end_date = handle_dates(args.d, args.y, args.m)
    issues = args.i
    write_path = args.f
    verbose = args.verbose_mode

    config_path = args.c
    script_location = os.path.dirname(getsourcefile(lambda: 0))
    install_directory = os.path.dirname(script_location)
    # If our config_path is a relative path, we make it relative to the
    # directory the scraper is installed in
    if not os.path.isabs(config_path):
        config_path = os.path.join(install_directory, config_path)

    journal_identifier = args.Journal_Identifier
    query_issn = get_issn(journal_identifier, config_path)

    if query_issn is None:
        print ("Unrecognized journal name or mistyped issn passed as an argument: " + journal_identifier)
        quit()

    # Here we get all the ISSNs a journal is published with
    journal_issns, journal_title = get_journal_info(query_issn)

    # And here we write all the articles that satisfy our criteria to our given file
    write_citations_for_issns(journal_issns, issues, journal_title, MAX_ITEMS_PER_PAGE, start_date, end_date, write_path)

if __name__ == "__main__":
    main(sys.argv[1:])
