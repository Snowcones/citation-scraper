# Returns a string of the elements in a list
def format_list(l):
    if l is None:
        return ""

    formatted_list = ""
    for element in l:
        formatted_list += str(element) + ", "
    formatted_list = formatted_list[:-2]
    return formatted_list

# Returns the name of a month given a month's integer
def month_string(month):
    months = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
    if month >= 1 and month <= 12:
        return months[month-1]
    else:
        print ("Month is now: " + str(month))
        raise ValueError(month)

# Returns a string in the form "Apr 2011" given a date of the form ['4', '2011']
def format_date(date):
    month = int(date[0])
    return month_string(month) + " " + date[1]

# Takes a list of authors in the form that CrossRef lists them and converts the list to a human readable form
def format_authors(authors):
    formatted_names = ""
    for i in range(len(authors)):
        formatted_names += authors[i]["given"] + " " + authors[i]["family"]
        if i != len(authors) - 1:
            formatted_names += " and "
    return formatted_names
