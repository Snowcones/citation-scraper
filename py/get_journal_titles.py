import sys, re

config_path = "config.txt"

with open(config_path, 'r') as config_file:
    config_text = config_file.read()
    journal_title_regex = r"(.*):"
    matches = re.findall(journal_title_regex, config_text)
    for i in range(len(matches)):
        sys.stdout.write(matches[i].replace(" ", "\\ "))
        if i != len(matches) - 1:
            sys.stdout.write(" ")
        sys.stdout.flush()
